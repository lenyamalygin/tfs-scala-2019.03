package tfs.naval.field

import tfs.naval.model.{Field, Ship}

class MockFieldController extends FieldController {

  override def validatePosition(ship: Ship, field: Field): Boolean = true

  override def markUsedCells(field: Field, ship: Ship): Field = field
}
